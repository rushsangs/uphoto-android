package com.example.ronishdesai.photoalbumapp;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;
import java.io.IOException;
import java.util.List;

public class MainPage extends AppCompatActivity {
    private static final String TAG = MainPage.class.getSimpleName();
    Context context;
    Session session;
    AlbumListAdapter adapter;
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = MainPage.this;
        session = new Session(context);
        setContentView(R.layout.activity_main_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                displayAlbumInput();
            }
        });

        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);

        adapter = new AlbumListAdapter(this, R.layout.content_album_item, session.sessionUser.getAlbums());
        adapter.notifyDataSetChanged();
        list = (ListView)findViewById(R.id.album_list);
        list.setAdapter(adapter);
        list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        list.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            Menu m;
            @Override
            public boolean onCreateActionMode(android.view.ActionMode actionMode, Menu menu) {
                SparseBooleanArray selected = adapter.getSelectedIds();
                actionMode.getMenuInflater().inflate(R.menu.main_page, menu);
                m = menu;
                return true;
            }

            @Override
            public boolean onPrepareActionMode(android.view.ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(android.view.ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_delete:
                        // Calls getSelectedIds method from ListViewAdapter Class
                        SparseBooleanArray selected = adapter.getSelectedIds();
                        // Captures all selected ids with a loop
                        for (int i = (selected.size() - 1); i >= 0; i--) {
                            if (selected.valueAt(i)) {
                                Album selectedItem = adapter.getItem(selected.keyAt(i));
                                try {
                                    if(session.deleteAlbum(selectedItem)){
                                        Toast.makeText(context, "Album successfully deleted", Toast.LENGTH_LONG).show();
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                // Remove selected items following the ids
                                adapter.remove(selectedItem);
                            }
                        }
                        // Close CAB
                        actionMode.finish();
                        return true;
                    case R.id.action_edit:
                        displayAlbumEdit(adapter.getItem(adapter.getSelectedIds().keyAt(0)));
                        adapter.toggleSelection(adapter.getSelectedIds().keyAt(0));
                        actionMode.finish();
                        return true;
                    default:
                        return false;
                }

            }

            @Override
            public void onDestroyActionMode(android.view.ActionMode actionMode) {
                final ListView lv = list;
                lv.clearChoices();
                for (int i = 0; i < lv.getCount(); i++)
                    lv.setItemChecked(i, false);
                lv.post(new Runnable() {
                    @Override
                    public void run() {
                        lv.setChoiceMode(ListView.CHOICE_MODE_NONE);
                    }
                });
                adapter.removeSelection();

            }

            @Override
            public void onItemCheckedStateChanged(android.view.ActionMode actionMode, int i, long l, boolean b) {
                // Capture total checked items
                final int checkedCount = list.getCheckedItemCount();
                m.findItem(R.id.action_move).setVisible(false);
                if(checkedCount==1) {
                    m.findItem(R.id.action_edit).setVisible(true);
                } else {
                    m.findItem(R.id.action_edit).setVisible(false);
                }
                // Set the CAB title according to total checked items
                actionMode.setTitle(checkedCount + " Selected");
                // Calls toggleSelection method from ListViewAdapter Class
                adapter.toggleSelection(i);
            }
        });


        list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    UserInfo.writeUser(session.sessionUser, context);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String selectedItem = session.sessionUser.getAlbums().get(position).getName();
                Toast.makeText(getApplicationContext(), selectedItem, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainPage.this, ImagePage.class);
                intent.putExtra("album", selectedItem);
                startActivity(intent);
            }
        });

    }

    public void displayAlbumInput() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainPage.this);
        builder.setTitle("Enter name of Album:");

        final EditText input = new EditText(MainPage.this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            String m_Text = null;
            public void onClick(DialogInterface dialog, int which) {
                try {
                    if(input.getText().toString().trim().equals("")) {
                        Toast.makeText(context, "Invalid Input", Toast.LENGTH_LONG).show();
                    } else if(session.createAlbum(input.getText().toString())) {
                        Log.d(TAG, "onNavigationItemSelected(): creation of album successful");
                        Toast.makeText(context, "Album successfully created", Toast.LENGTH_LONG).show();
                    } else {
                        Log.d(TAG, "onNavigationItemSelected(): creation of album unsuccessful");
                        Toast.makeText(context, "Album already exists", Toast.LENGTH_LONG).show();
                    }
                    adapter.notifyDataSetChanged();
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void displayAlbumEdit(final Album album) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainPage.this);
        builder.setTitle("Enter new name of Album:");

        final EditText input = new EditText(MainPage.this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(album.getName());
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            String m_Text = null;
            public void onClick(DialogInterface dialog, int which) {
                try {
                    if(input.getText().toString().trim().equals("")) {
                        Toast.makeText(context, "Invalid Input", Toast.LENGTH_LONG).show();
                    } else if(session.editAlbum(album, input.getText().toString())){
                        Toast.makeText(context, "Album name editted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(context, "Album name already exists", Toast.LENGTH_LONG).show();
                    }
                    adapter.notifyDataSetChanged();
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_button, menu);
        return true;
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.search_menu, menu);
//        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                callSearch(query);
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
////              if (searchView.isExpanded() && TextUtils.isEmpty(newText)) {
//                updateSuggestions(newText);
////              }
//                return true;
//            }
//
//            public void callSearch(String query) {
//
//                Log.d("QUERY", query + " is the search query");
//                List<MyImage> results = session.sessionUser.searchImages(query);
//                onSearchRequested();
////                Intent searchIntent = new Intent(MainPage.this, SearchableActivity.class);
////                searchIntent.setAction(Intent.ACTION_SEARCH);
////                startActivity(searchIntent);
//            }
//            public void updateSuggestions(String text){
//                List<String> suggestions = session.sessionUser.getSuggestions(text);
//            }
//
//        });
//        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_edit) {
            return true;
        } else if (id == R.id.action_delete) {
            return true;
        } else if (id == R.id.action_search_button){
            Intent intent = new Intent(MainPage.this, SearchPage.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            UserInfo.writeUser(session.sessionUser, context);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        try {
            UserInfo.writeUser(session.sessionUser, context);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
