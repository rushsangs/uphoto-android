package com.example.ronishdesai.photoalbumapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.CursorLoader;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class ImagePage extends AppCompatActivity {
    private static final String TAG = MainPage.class.getSimpleName();
    Context context;
    Session session;
    ImageListAdapter adapter;
    String albumName = null;
    GridView gridView;
    private static int IMAGE_GALLERY_REQUEST = 20;
    String imgDecodableString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = ImagePage.this;
        session = new Session(context);
        setContentView(R.layout.activity_image_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            albumName = extras.getString("album");
        }

        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                loadImageFromGallery(view);
            }
        });

        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);

        adapter = new ImageListAdapter(this, R.layout.content_image_item, session.sessionUser.getAlbum(albumName).getImages());
        adapter.notifyDataSetChanged();
        gridView = (GridView)findViewById(R.id.image_grid);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
//              MyImage item = (MyImage) parent.getItemAtPosition(position);
                Intent intent = new Intent(context, DetailsPage.class);
                intent.putExtra("albumName", albumName);
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });

        gridView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        gridView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            Menu m;
            @Override
            public boolean onCreateActionMode(android.view.ActionMode actionMode, Menu menu) {
                actionMode.getMenuInflater().inflate(R.menu.main_page, menu);
                m = menu;
                return true;
            }

            @Override
            public boolean onPrepareActionMode(android.view.ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(android.view.ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_delete:
                        // Calls getSelectedIds method from ListViewAdapter Class
                        SparseBooleanArray selected = adapter.getSelectedIds();
                        // Captures all selected ids with a loop
                        for (int i = (selected.size() - 1); i >= 0; i--) {
                            if (selected.valueAt(i)) {
                                MyImage selectedItem = adapter.getItem(selected.keyAt(i));
                                try {
                                    session.deleteImage(session.sessionUser.getAlbum(albumName), selectedItem);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                // Remove selected items following the ids
                                adapter.remove(selectedItem);
                            }
                        }
                        // Close CAB
                        actionMode.finish();
                        return true;
                    case R.id.action_edit:
                        displayImageEdit(adapter.getItem(adapter.getSelectedIds().keyAt(0)));
                        adapter.toggleSelection(adapter.getSelectedIds().keyAt(0));
                        actionMode.finish();
                        return true;
                    case R.id.action_move:
                        displayMoveImage(adapter.getItem(adapter.getSelectedIds().keyAt(0)));
                        adapter.toggleSelection(adapter.getSelectedIds().keyAt(0));
                        actionMode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(android.view.ActionMode actionMode) {
                final GridView lv = gridView;
                lv.clearChoices();
                for (int i = 0; i < lv.getCount(); i++)
                    lv.setItemChecked(i, false);
                lv.post(new Runnable() {
                    @Override
                    public void run() {
                        lv.setChoiceMode(ListView.CHOICE_MODE_NONE);
                    }
                });
                adapter.removeSelection();
            }

            @Override
            public void onItemCheckedStateChanged(android.view.ActionMode actionMode, int i, long l, boolean b) {
                // Capture total checked items
                final int checkedCount = gridView.getCheckedItemCount();
                if(checkedCount==1) {
                    m.findItem(R.id.action_move).setVisible(true);
                    m.findItem(R.id.action_edit).setVisible(true);
                } else {
                    m.findItem(R.id.action_move).setVisible(false);
                    m.findItem(R.id.action_edit).setVisible(false);
                }
                // Set the CAB title according to total checked items
                actionMode.setTitle(checkedCount + " Selected");
                // Calls toggleSelection method from ListViewAdapter Class
                adapter.toggleSelection(i);
            }
        });
    }

    public void displayMoveImage(final MyImage image){
        if(session.sessionUser.getAlbums().size() == 1){
            Toast.makeText(context, "Cannot move since there are no addition albums", Toast.LENGTH_LONG).show();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(ImagePage.this);
        builder.setTitle("Enter new name of Image:");

        final ArrayAdapter<Album> arrayAdapter = new ArrayAdapter<Album>(ImagePage.this, android.R.layout.select_dialog_singlechoice);
        for(Album album: session.sessionUser.getAlbums()){
            if(!album.getName().equals(albumName))
                arrayAdapter.add(album);
        }
        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final Album selected = arrayAdapter.getItem(which);
                AlertDialog.Builder builderInner = new AlertDialog.Builder(ImagePage.this);
                assert selected != null;
                builderInner.setMessage(selected.getName());
                builderInner.setTitle("Your Selected Album is:");
                builderInner.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            Album temp = session.sessionUser.getAlbum(albumName);
                            if (session.moveImage(temp, image, selected)){
                                Log.d(TAG, "onNavigationItemSelected(): moving of image successful");
                                Toast.makeText(context, "Image moved successfully", Toast.LENGTH_LONG).show();
                            } else {
                                Log.d(TAG, "onNavigationItemSelected(): moving of image unsuccessful");
                                Toast.makeText(context, "Image could not be moved", Toast.LENGTH_LONG).show();
                            }
                            adapter.notifyDataSetChanged();
                            adapter.notifyDataSetChanged();
                            dialog.dismiss();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builderInner.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builderInner.show();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void displayImageInput(){
        AlertDialog.Builder builder = new AlertDialog.Builder(ImagePage.this);
        builder.setTitle("Enter name of Image:");

        final EditText input = new EditText(ImagePage.this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            String m_Text = null;
            public void onClick(DialogInterface dialog, int which) {
                try {
                    if(input.getText().toString().trim().equals("")) {
                        Toast.makeText(context, "Invalid Input", Toast.LENGTH_LONG).show();
                    } else if (session.addImage(session.sessionUser.getAlbum(albumName), input.getText().toString(), imgDecodableString)) {
                        Log.d(TAG, "onNavigationItemSelected(): creation of image successful");
                        Toast.makeText(context, "Image successfully created", Toast.LENGTH_LONG).show();
                    } else {
                        Log.d(TAG, "onNavigationItemSelected(): creation of image unsuccessful");
                        Toast.makeText(context, "Image already exists", Toast.LENGTH_LONG).show();
                    }
                    adapter.notifyDataSetChanged();
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void displayImageEdit(final MyImage image) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ImagePage.this);
        builder.setTitle("Enter new name of Image:");

        final EditText input = new EditText(ImagePage.this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(image.getCaption());
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            String m_Text = null;
            public void onClick(DialogInterface dialog, int which) {
                try {
                    if(input.getText().toString().trim().equals("")) {
                        Toast.makeText(context, "Invalid Input", Toast.LENGTH_LONG).show();
                    } else if(session.editImage(session.sessionUser.getAlbum(albumName), input.getText().toString(), image)){
                        Toast.makeText(context, "Image name editted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(context, "Image name already exists", Toast.LENGTH_LONG).show();
                    }
                    adapter.notifyDataSetChanged();
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void loadImageFromGallery(View view) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK);
        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDirectory.getPath();

        Uri data = Uri.parse(pictureDirectoryPath);
        galleryIntent.setDataAndType(data, "image/*");

        startActivityForResult(galleryIntent, IMAGE_GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if(requestCode == IMAGE_GALLERY_REQUEST && resultCode == RESULT_OK && data != null) {

                Uri selectedImage = data.getData();
                imgDecodableString = getRealPathFromURI(selectedImage);

                displayImageInput();
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }

    public String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
//        getMenuInflater().inflate(R.menu.search_menu, menu);
//        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
//        //searchView.setOnQueryTextListener(this);
//        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_edit) {
            return true;
        } else if (id == R.id.action_delete) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            UserInfo.writeUser(session.sessionUser, context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostResume() {
        adapter.notifyDataSetChanged();
        adapter.notifyDataSetChanged();
        super.onPostResume();
    }

    @Override
    public void onBackPressed() {
        try {
            UserInfo.writeUser(session.sessionUser, context);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(ImagePage.this, MainPage.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
