package com.example.ronishdesai.photoalbumapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import java.io.IOException;
import java.util.ArrayList;

public class DetailsPage extends AppCompatActivity {
    private static final String TAG = MainPage.class.getSimpleName();
    Context context;
    Session session;
    ImageSwitcher sw;
    Button b1, b2;
    Bitmap selectedImg;
    ArrayList<String> selectedTags;
    ListView tagsList;
    TagListAdapter adapter;
    int position;
    String albumName;
    EditText person;
    EditText location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = DetailsPage.this;
        session = new Session(context);
        setContentView(R.layout.activity_display_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        position = getIntent().getIntExtra("position", -1);
        albumName = getIntent().getStringExtra("albumName");
        if(position != -1) {
            selectedImg = BitmapFactory.decodeFile(session.sessionUser.getAlbum(albumName).getImages().get(position).getImagePath());
            selectedTags = session.sessionUser.getAlbum(albumName).getImages().get(position).getTags();
        }

        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);

//        TextView titleTextView = (TextView) findViewById(R.id.title);
//        titleTextView.setText(title);

        b1 = (Button) findViewById(R.id.back_button);
        b2 = (Button) findViewById(R.id.forward_button);

        sw = (ImageSwitcher) findViewById(R.id.display_image);
        sw.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView myView = new ImageView(getApplicationContext());
                myView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                myView.setLayoutParams(new
                        ImageSwitcher.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                        ActionBar.LayoutParams.MATCH_PARENT));
                return myView;
            }
        });
        sw.setImageDrawable(new BitmapDrawable(getResources(), selectedImg));

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position > 0){
                    position--;
                }
                selectedImg = BitmapFactory.decodeFile(session.sessionUser.getAlbum(albumName).getImages().get(position).getImagePath());
                sw.setImageDrawable(new BitmapDrawable(getResources(), selectedImg));
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position < session.sessionUser.getAlbum(albumName).getImages().size()-1) {
                    position++;
                }
                selectedImg = BitmapFactory.decodeFile(session.sessionUser.getAlbum(albumName).getImages().get(position).getImagePath());
                sw.setImageDrawable(new BitmapDrawable(getResources(), selectedImg));
            }
        });

        adapter = new TagListAdapter(this, R.layout.content_display_item, session.sessionUser.getAlbum(albumName).getImages().get(position).getTags());
        adapter.notifyDataSetChanged();
        tagsList = (ListView)findViewById(R.id.tag_list);
        tagsList.setAdapter(adapter);
        tagsList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        tagsList.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            Menu m;
            @Override
            public boolean onCreateActionMode(android.view.ActionMode actionMode, Menu menu) {
                SparseBooleanArray selected = adapter.getSelectedIds();
                actionMode.getMenuInflater().inflate(R.menu.main_page, menu);
                m = menu;
                return true;
            }

            @Override
            public boolean onPrepareActionMode(android.view.ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(android.view.ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_delete:
                        // Calls getSelectedIds method from ListViewAdapter Class
                        SparseBooleanArray selected = adapter.getSelectedIds();
                        // Captures all selected ids with a loop
                        for (int i = (selected.size() - 1); i >= 0; i--) {
                            if (selected.valueAt(i)) {
                                String selectedItem = adapter.getItem(selected.keyAt(i));
                                try {
                                    Album tmpAlbum = session.sessionUser.getAlbum(albumName);
                                    if(session.removeTag(tmpAlbum, tmpAlbum.getImages().get(position), selectedItem)){
                                        Toast.makeText(context, "Tag successfully deleted", Toast.LENGTH_LONG).show();
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                // Remove selected items following the ids
                                adapter.remove(selectedItem);
                            }
                        }
                        // Close CAB
                        actionMode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(android.view.ActionMode actionMode) {
                
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onItemCheckedStateChanged(android.view.ActionMode actionMode, int i, long l, boolean b) {
                // Capture total checked items
                final int checkedCount = tagsList.getCheckedItemCount();
                m.findItem(R.id.action_edit).setVisible(false);
                m.findItem(R.id.action_move).setVisible(false);
                // Set the CAB title according to total checked items
                actionMode.setTitle(checkedCount + " Selected");
                // Calls toggleSelection method from ListViewAdapter Class
                adapter.toggleSelection(i);
            }
        });
    }

    public void displayImageInput(){
        AlertDialog.Builder builder = new AlertDialog.Builder(DetailsPage.this);
        builder.setTitle("Enter the tag: (Use ',' for more tags)");
        person = new EditText(context);
        person.setText("Enter person tags here");
        person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                person.setText("");
            }
        });
        location = new EditText(context);
        location.setText("Enter location tags here");
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                location.setText("");
            }
        });

        person.setInputType(InputType.TYPE_CLASS_TEXT);
        location.setInputType(InputType.TYPE_CLASS_TEXT);

        LinearLayout ll=new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.addView(person);
        ll.addView(location);
        builder.setView(ll);

        builder.setCancelable(false);
        builder.setPositiveButton("OK",  new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                try {
                    int p_error = 0;
                    int l_error = 0;
                    String newTag;
                    Album temp = session.sessionUser.getAlbum(albumName);
                    if(!person.getText().toString().equals("Enter person tags here")) {
                        if (!person.getText().toString().trim().equals("")) {
                            String[] people = person.getText().toString().split(",");
                            for (String p : people) {
                                newTag = "person-" + p.trim();
                                session.aggTag(temp, temp.getImages().get(position), newTag);
                                p_error = 1;
                            }
                        } else {
                            p_error = -1;
                        }
                    }
                    if(!location.getText().toString().equals("Enter location tags here")){
                        if(!location.getText().toString().trim().equals("")){
                            String[] locations = location.getText().toString().split(",");
                            for(String p : locations){
                                newTag = "location-" + p.trim();
                                session.aggTag(temp, temp.getImages().get(position), newTag);
                                l_error = 1;
                            }
                        } else {
                            l_error = -1;
                        }
                    }

                    if(p_error == 1 && l_error == 1) {
                        Toast.makeText(context, "Tags added successfully", Toast.LENGTH_LONG).show();
                    } else if(p_error == 1){
                        Toast.makeText(context, "Person tag added successfully", Toast.LENGTH_LONG).show();
                    } else if(l_error == 1){
                        Toast.makeText(context, "Location tag added successfully", Toast.LENGTH_LONG).show();
                    }

                    if(p_error == -1 && l_error == -1){
                        Toast.makeText(context, "Person and location tags were empty", Toast.LENGTH_LONG).show();
                    } else if(p_error == -1){
                        Toast.makeText(context, "Person tag empty", Toast.LENGTH_LONG).show();
                    } else if(l_error == -1){
                        Toast.makeText(context, "Location tag empty", Toast.LENGTH_LONG).show();
                    }

//                    if (session.aggTag(temp, temp.getImages().get(position), newTag)) {
//                        Log.d(TAG, "onNavigationItemSelected(): creation of image successful");
//                        Toast.makeText(context, "Tag successfully created", Toast.LENGTH_LONG).show();
//                    } else {
//                        Log.d(TAG, "onNavigationItemSelected(): creation of image unsuccessful");
//                        Toast.makeText(context, "Tag already exists", Toast.LENGTH_LONG).show();
//                    }
                    adapter.notifyDataSetChanged();
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.display_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if(item.getItemId() == R.id.action_add_tag){
            displayImageInput();
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            UserInfo.writeUser(session.sessionUser, context);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            UserInfo.writeUser(session.sessionUser, context);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(DetailsPage.this, ImagePage.class);
        intent.putExtra("album", albumName);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
