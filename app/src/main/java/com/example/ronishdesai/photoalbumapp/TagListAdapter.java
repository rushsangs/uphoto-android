package com.example.ronishdesai.photoalbumapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class TagListAdapter extends ArrayAdapter<String>{
    private Context context;
    private int layoutResourceId;
    private ArrayList<String> data;
    private SparseBooleanArray mSelectedItemsIds;
    private ArrayList<Integer> selectedIds = new ArrayList<Integer>();

    public TagListAdapter(Context context, int layoutResourceId, ArrayList<String> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.mSelectedItemsIds = new SparseBooleanArray();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ViewHolder();
            holder.tagType = (TextView)row.findViewById(R.id.tag_person);
            holder.tagValue = (TextView)row.findViewById(R.id.tag_location);

            row.setTag(holder);
        } else {
            holder = (ViewHolder)row.getTag();
            if (selectedIds.contains(position)) {
                row.setSelected(true);
                row.setPressed(true);
                row.setBackgroundColor(Color.parseColor("#FF9912"));
            } else {
                row.setSelected(false);
                row.setPressed(false);
                row.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
        }

        if(data != null) {
            String item = data.get(position);
            if(item != null){
                String type = item.substring(0, item.indexOf("-"));
                String value = item.substring(item.indexOf("-"));
                holder.tagType.setText(type);
                holder.tagValue.setText(value);
            }
        }

        return row;
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        selectedIds = new ArrayList<Integer>();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        if(selectedIds.contains(position)) {
            selectedIds.remove(selectedIds.indexOf(position));
        } else {
            selectedIds.add(position);
        }
        notifyDataSetChanged();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    static class ViewHolder {
        TextView tagType;
        TextView tagValue;
    }
}
