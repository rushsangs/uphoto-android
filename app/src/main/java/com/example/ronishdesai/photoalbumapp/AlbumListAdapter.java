package com.example.ronishdesai.photoalbumapp;

import android.app.Activity;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;

import java.util.ArrayList;

class AlbumListAdapter extends ArrayAdapter<Album>{
    private Context context;
    private ArrayList<Album> itemname;
    private int layoutResourceId;
    private SparseBooleanArray mSelectedItemsIds;
    private ArrayList<Integer> selectedIds = new ArrayList<Integer>();

    AlbumListAdapter(Context context, int layoutResourceId, ArrayList<Album> itemname) {
        super(context, layoutResourceId, itemname);
        this.context = context;
        this.mSelectedItemsIds = new SparseBooleanArray();
        this.layoutResourceId = layoutResourceId;
        this.itemname = itemname;
    }

    public View getView(int position, View view, ViewGroup parent) {
        View rowView = view;
        AlbumHolder holder = null;

        if(view == null){
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            rowView = inflater.inflate(layoutResourceId, parent, false);

            holder = new AlbumHolder();
            holder.imgIcon = (ImageView)rowView.findViewById(R.id.album_icon);
            holder.txtTitle = (TextView)rowView.findViewById(R.id.album_title);
            holder.txtDetail = (TextView)rowView.findViewById(R.id.album_detail);

            rowView.setTag(holder);
        } else {
            holder = (AlbumHolder)rowView.getTag();
            if (selectedIds.contains(position)) {
                view.setSelected(true);
                view.setPressed(true);
                view.setBackgroundColor(Color.parseColor("#FF9912"));
            } else {
                view.setSelected(false);
                view.setPressed(false);
                view.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
        }

        if(itemname != null){
            holder.txtTitle.setText(itemname.get(position).getName());
            holder.txtDetail.setText(itemname.get(position).getName());
        }

        return rowView;

    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        selectedIds = new ArrayList<Integer>();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        if(selectedIds.contains(position)) {
            selectedIds.remove(selectedIds.indexOf(position));
        } else {
            selectedIds.add(position);
        }
        notifyDataSetChanged();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    static class AlbumHolder {
        ImageView imgIcon;
        TextView txtTitle;
        TextView txtDetail;
    }
}
