package com.example.ronishdesai.photoalbumapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The Album Class stores all the information regarding all the albums under the given user
 * 
 * @author Ronish Desai, Rushit Sanghrajka
 *
 */
class Album implements Serializable{
	private static final long serialVersionUID = 1L;
	private String name;
	private ArrayList<MyImage> imageList;
	
	public Album(String name){
		this.name = name;
		imageList = new ArrayList<MyImage>() ;
	}
	
	/**
	 * Gets the name of the album
	 * @return name of the album
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * Sets the name of the album
	 * @param name name of the album
	 */
	public void setName(String name){
		this.name = name;
	}
	
	/**
	 * Gets the list of images that belong to the album
	 * @return List of images
	 */
	public ArrayList<MyImage> getImages(){ return imageList; }
	
	/**
	 * Sets the list of images to the current album
	 * @param imageList ObservalbleList of images
	 */
	public void setImages(ArrayList<MyImage> imageList){
		this.imageList = imageList;
	}
	
	/**
	 * Gets the image based on the caption
	 * @param name caption of the image
	 * @return MyImage with the specified caption
	 */
	public MyImage getImage(String name){
		for(int i = 0; i < imageList.size(); i++){
			if(imageList.get(i).getCaption().equals(name)){
				return imageList.get(i);
			}
		}
		return null;
	}
	
	/**
	 * Adds the given image to the image list based on the whether the list contains the caption
	 * @param image image that is to be added
	 * @return true if it was added, false otherwise
	 */
	public boolean addImage(MyImage image){
		if(imageList.contains(image)){
			return false;
		} else {
			imageList.add(image);
			return true;
		}
	}
	
	/**
	 * Adds the given image to the image list regardless of it existing in the list already
	 * @param image image that is to be added
	 * @return true 
	 */
	public boolean addImageForce(MyImage image){
		imageList.add(image);
		return true;
	}
	
	/**
	 * Deletes the given image from the image list
	 * @param image image that is to be added
	 * @return true if it was deleted, false otherwise
	 */
	public boolean deleteImage(MyImage image){
		return imageList.remove(image);
	}
	
	/**
	 * Edits the given image 
	 * @param image image to be edited 
	 * @param name new caption
	 * @return true if it was updated, false otherwise
	 */
	public boolean editImage(MyImage image, String name){
		if(imageList.remove(image)){
			image.setCaption(name);
			return addImage(image);
		} else {
			imageList.add(image);
			return false;
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Album)
		{
			if(((Album)obj).name.equals(this.name))
				return true;
			return false;
		}
		return false;
	}
	@Override
	public String toString() {
		return this.name;
	}
}
