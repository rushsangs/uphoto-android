package com.example.ronishdesai.photoalbumapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Stores user information
 * @author Ronish Desai, Rushit Sanghrajka
 *
 */
public class User implements Serializable {
	private static final long serialVersionUID = 1779210462873645200L;
	private ArrayList<Album> albums;
	//TODO Users also will store information about albums, etc.!
	
	public User() {
		albums = new ArrayList<Album>();
	}
	
	/**
	 * Gets the list of albums of the user
	 * @return ObservableList of Albums
	 */
	public ArrayList<Album> getAlbums(){
		return albums;
	}
	
	/**
	 * Gets the album given the album name
	 * @param name the name of the album
	 * @return the album with the given name
	 */
	public Album getAlbum(String name){
		for(int i = 0; i < albums.size(); i++){
			if(albums.get(i).getName().equals(name)){
				return albums.get(i);
			}
		}
		return null;
	}
	
	/**
	 * Sets the album list to the specified ArrayList
	 * @param albums the new ArrayList of albums
	 */
	public void setAlbums(ArrayList<Album> albums){
		this.albums = albums;
	}
	
	/**
	 * Adds the given album to the album list
	 * @param temp new album that is needed to be added
	 * @return true if it was added, false otherwise 
	 */
	public boolean addAlbum(Album temp){
		if(albums.contains(temp)){
			return false;
		} else {
			albums.add(temp);
			return true;
		}	
	}
	
	/**
	 * Edits the name of the given album
	 * @param temp the specified album
	 * @param name the new name of the album
	 * @return true if it was edited, false otherwise
	 */
	public boolean editAlbum(Album temp, String name){
		if(albums.remove(temp)){
			for(int i = 0; i < albums.size(); i++){
				if(albums.get(i).getName().equals(name)){
					albums.add(temp);
					return false;
				}
			}
			temp.setName(name);
			albums.add(temp);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Deletes the specified album
	 * @param temp the album that is to be deleted
	 * @return true if it was delete, false otherwisw 
	 */
	public boolean deleteAlbum(Album temp){
		return albums.remove(temp);	
	}

	public ArrayList<MyImage> searchImages(String query){
		ArrayList<MyImage> results = new ArrayList<MyImage>();
		if(query==null)
			return results;
		for(Album album : albums){
			for(MyImage img : album.getImages()){
				for(String tag : img.getTags()){
					if(tag.indexOf(query)!= -1)
						results.add(img);
				}
			}
		}
		return results;
	}

	public List<String> getSuggestions(String query){
		List<String> results = new ArrayList<String>();
		for(Album album : albums){
			for(MyImage img : album.getImages()){
				for(String tag : img.getTags()){
					if(tag.indexOf(query)!= -1)
						results.add(tag.substring(tag.indexOf("-")+1));
				}
			}
		}
		return results;
	}

}
