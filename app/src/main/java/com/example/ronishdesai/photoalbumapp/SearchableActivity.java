package com.example.ronishdesai.photoalbumapp;

import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SearchableActivity extends ListActivity {
    Context context;
    Session session;
    ArrayList<MyImage> data;

//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        context = SearchableActivity.this;
//        session =  new Session(context);
//        handleIntent(getIntent());
//
//        data = new ArrayList<MyImage>();
//        adapter = new ImageListAdapter(this, R.layout.content_image_item, data);
//        adapter.notifyDataSetChanged();
//        gridView = (GridView)findViewById(R.id.image_grid);
//        gridView.setAdapter(adapter);
//        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
////              MyImage item = (MyImage) parent.getItemAtPosition(position);
////                Intent intent = new Intent(context, DetailsPage.class);
////                intent.putExtra("position", position);
////                startActivity(intent);
//            }
//        });
//
//    }

    public void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        // call detail activity for clicked entry
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Toast.makeText(context, query, Toast.LENGTH_LONG).show();
            doSearch(query);
        }
    }
    private void doSearch(String queryStr) {
        data = session.sessionUser.searchImages(queryStr);
        // get a Cursor, prepare the ListAdapter
        // and set it
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = SearchableActivity.this;
        session = new Session(context);
        Log.d("QUERY", "Search activity begins");
        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.d("QUERY", query + " is the search query");

            Intent intent2 = new Intent(SearchableActivity.this, SearchPage.class);
            intent2.setAction(Intent.ACTION_ANSWER);
            intent2.putExtra("query", query);
            startActivity(intent2);

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the options menu from XML
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        if(null!=searchManager ) {
//            Log.d("Search", "manager is null");
//            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        }
//        searchView.setIconifiedByDefault(false);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onSearchRequested() {
        Log.d("SEARCH","onSearchRequested");
        return super.onSearchRequested();
    }
}
