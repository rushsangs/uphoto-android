package com.example.ronishdesai.photoalbumapp;

import android.graphics.Bitmap;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * The MyImage Class stores all the information regarding all the images under the given album of a given user
 * 
 * @author Ronish Desai, Rushit Sanghrajka
 *
 */
 class MyImage implements Serializable {

	private static final long serialVersionUID = 1L;
	private String caption;
	private ArrayList<String> tags;
	private String imagePath;
	
	public MyImage(String caption, String filePath){	//date format: dd/mm/yyyy
		this.caption = caption;
		imagePath = filePath;
		tags = new ArrayList<String>();
	}
	
	/**
	 * Gets the caption of the image
	 * @return caption of the image
	 */
	public String getCaption(){
		return caption;
	}
	
	/**
	 * Sets the caption of the image
	 * @param title caption of the image
	 */
	public void setCaption(String title){
		caption = title;
	}

	/**
	 * Returns the list of tags
	 * @return ArrayList<String> tag
     */
	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public boolean addTag(String tag){
		if(tags.contains(tag)){
			return false;
		} else {
			return tags.add(tag);
		}
	}

	public boolean removeTag(String tag){
		return tags.remove(tag);
	}


	/**
	 * Gets the image that is stored in MyImage
	 * @return returns the file containing the image
	 */
	public String getImagePath(){
		return imagePath;
	}
	
	/**
	 * Sets the image to the specified image file
	 * @param f an image file
	 */
	public void setImagePath(String f){
		imagePath = f;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof MyImage)
		{
			if(((MyImage)obj).caption.equals(this.caption))
				return true;
			return false;
		}
		return false;
	}
	@Override
	public String toString() {
		return this.caption;
	}

}
