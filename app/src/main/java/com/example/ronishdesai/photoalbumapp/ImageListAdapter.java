package com.example.ronishdesai.photoalbumapp;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;
import java.util.ArrayList;

public class ImageListAdapter extends ArrayAdapter<MyImage>{
    private Context context;
    private int layoutResourceId;
    private ArrayList<MyImage> data;
    private SparseBooleanArray mSelectedItemsIds;
    private ArrayList<Integer> selectedIds = new ArrayList<Integer>();

    public ImageListAdapter(Context context, int layoutResourceId, ArrayList<MyImage> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.mSelectedItemsIds = new SparseBooleanArray();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ViewHolder();
            holder.imageTitle = (TextView)row.findViewById(R.id.image_text);
            holder.image = (ImageView)row.findViewById(R.id.image);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
            if (selectedIds.contains(position)) {
                row.setSelected(true);
                row.setPressed(true);
                row.setBackgroundColor(Color.parseColor("#FF9912"));
            } else {
                row.setSelected(false);
                row.setPressed(false);
                row.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
        }

        if(data != null) {
            MyImage item = (MyImage)data.get(position);
            holder.imageTitle.setText(item.getCaption());
            holder.image.setImageBitmap(BitmapFactory.decodeFile(item.getImagePath()));
            holder.image.setScaleType(ImageView.ScaleType.FIT_CENTER);
        }

        return row;
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        if(selectedIds.contains(position)) {
            selectedIds.remove(selectedIds.indexOf(position));
        } else {
            selectedIds.add(position);
        }
        notifyDataSetChanged();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    static class ViewHolder {
        TextView imageTitle;
        ImageView image;
    }
}
