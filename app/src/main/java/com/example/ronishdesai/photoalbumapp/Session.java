package com.example.ronishdesai.photoalbumapp;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * The session class stores all the executions that are done by a user and serialzes the changes
 * @author Ronish Desai, Rushit Sanghrajka
 */
public class Session {
	public User sessionUser;
	Context context;
	
	public Session(Context context){
		this.context = context;
		try {
			sessionUser = UserInfo.readUser(context);
			if(sessionUser==null)
				sessionUser = new User();
		} catch (NullPointerException e){
			sessionUser = new User();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates an ablum given the name
	 * @param name name of the album
	 * @return true if it was created, false otherwise
	 * @throws IOException
	 */
	public boolean createAlbum(String name)  throws IOException {
		UserInfo.writeUser(null, context);
		Album album = new Album(name);
		if(sessionUser.addAlbum(album)){
			UserInfo.writeUser(sessionUser, context);
			return true;
		} else {
			UserInfo.writeUser(sessionUser, context);
			return false;
		}
	}
	
	/**
	 * Creates an album given the name and list of images
	 * @param name name of the album
	 * @param images Arraylist of MyImage
	 * @return true if it was created, false otherwise
	 * @throws IOException
	 */
	public boolean createAlbum(String name, ArrayList<MyImage> images) throws IOException{
		UserInfo.writeUser(null, context);
		Album album = new Album(name);
		album.setImages(images);
		if(sessionUser.addAlbum(album)){
			UserInfo.writeUser(sessionUser, context);
			return true;
		} else {
			UserInfo.writeUser(sessionUser, context);
			return false;
		}
	}

	/**
	 * Edits the album given the specified name 
	 * @param selected the selected album that is to be edited
	 * @param name the new name of the album
	 * @return true if it was edited, false otherwise
	 * @throws IOException
	 */
	public boolean editAlbum(Album selected, String name) throws IOException{
		UserInfo.writeUser(null, context);
		if(sessionUser.editAlbum(selected, name)){
			UserInfo.writeUser(sessionUser, context);
			return true;
		} else {
			UserInfo.writeUser(sessionUser, context);
			return false;
		}
	}
	
	/**
	 * Deletes the album given the specified album
	 * @param selected the album that is to be deleted
	 * @return true if it was deleted, false otherwise
	 * @throws IOException
	 */
	public boolean deleteAlbum(Album selected) throws IOException{
		UserInfo.writeUser(null, context);
		if(sessionUser.deleteAlbum(selected)){
			UserInfo.writeUser(sessionUser, context);
			return true;
		} else {
			UserInfo.writeUser(sessionUser, context);
			return false;
		}
	}
	
	/**
	 * Adds an image to the specified album
	 * @param album the album to which the image should be added to
	 * @param name the caption of the image
	 * @param filePath the file containing the image
	 * @return true if it was added, false otherwise
	 * @throws IOException
	 */
	public boolean addImage(Album album, String name, String filePath) throws IOException {
		UserInfo.writeUser(null, context);
		Album selected = sessionUser.getAlbum(album.getName());
		MyImage image = new MyImage(name, filePath);
		if(selected.addImage(image)){
			UserInfo.writeUser(sessionUser, context);
			return true;
		} else {
			UserInfo.writeUser(sessionUser, context);
			return false;
		}
	}
	
	/**
	 * Deletes an image from the particular album
	 * @param album the specified album
	 * @param image the specified image
	 * @return true if it was deleted, false otherwise
	 * @throws IOException
	 */
	public boolean deleteImage(Album album, MyImage image) throws IOException {
		UserInfo.writeUser(null, context);
		Album selected = sessionUser.getAlbum(album.getName());
		if(selected.deleteImage(image)){
			UserInfo.writeUser(sessionUser, context);
			return true;
		} else {
			UserInfo.writeUser(sessionUser, context);
			return false;
		}
	}
	
	/**
	 * Edits the image given new set of information
	 * @param album the album that contains the image
	 * @param caption the caption of the image
	 * @param image the file containing the image
	 * @return true if it was edited, false otherwise
	 * @throws IOException
	 */
	public boolean editImage(Album album, String caption, MyImage image) throws IOException {
		UserInfo.writeUser(null, context);
		Album selected = sessionUser.getAlbum(album.getName());
		if(selected.editImage(image, caption)){
			UserInfo.writeUser(sessionUser, context);
			return true;
		} else {
			UserInfo.writeUser(sessionUser, context);
			return false;
		}
	}
	
	/**
	 * Moves the specified image from the old album to the new album
	 * @param album the previous album
	 * @param image the image
	 * @param newAlbum the new album the image should be moved to
	 * @return true if it was moved, false otherwise
	 * @throws IOException
	 */
	public boolean moveImage(Album album, MyImage image, Album newAlbum) throws IOException{
		UserInfo.writeUser(null, context);
		Album selected = sessionUser.getAlbum(album.getName());
		if(selected.deleteImage(image)){
			if(newAlbum.addImage(image)) {
				UserInfo.writeUser(sessionUser, context);
				return true;
			} else {
				if(selected.addImage(image)){
					UserInfo.writeUser(sessionUser, context);
					return false;
				} else {
					System.out.println("Fatal Error!");
					return false;
				}
			}
		} else {
			UserInfo.writeUser(sessionUser, context);
			return false;
		}
	}
	
	/**
	 * Copys the image to the specified album
	 * @param image the image
	 * @param newAlbum the album that the image should be copied to
	 * @return true if it was copied, false otherwise
	 * @throws IOException
	 */
	public boolean copyImage(MyImage image, Album newAlbum) throws IOException{
		UserInfo.writeUser(null, context);
		if(newAlbum.addImageForce(image)) {
			UserInfo.writeUser(sessionUser, context);
			return true;
		} else {
			UserInfo.writeUser(sessionUser, context);
			return false;
		}
	}

	public boolean aggTag(Album tmpAlbum, MyImage tmp, String tag) throws IOException {
		UserInfo.writeUser(null, context);
		if(tmpAlbum.getImage(tmp.getCaption()).addTag(tag)){
			UserInfo.writeUser(sessionUser, context);
			return true;
		} else {
			UserInfo.writeUser(sessionUser, context);
			return true;
		}
	}

	public boolean removeTag(Album tmpAlbum, MyImage tmp, String tag) throws IOException {
		UserInfo.writeUser(null, context);
		if(tmpAlbum.getImage(tmp.getCaption()).removeTag(tag)){
			UserInfo.writeUser(sessionUser, context);
			return true;
		} else {
			UserInfo.writeUser(sessionUser, context);
			return true;
		}
	}

}
