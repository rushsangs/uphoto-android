package com.example.ronishdesai.photoalbumapp;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The UserList Class stores all user functionalities and serializes them
 * 
 * @author Ronish Desai, Rushit Sanghrajka
 *
 */
public class UserInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String TAG = UserInfo.class.getSimpleName();
	private static final String storeDir = ".";
	private static final String storeFile = "user.dat";

	/**
	 * Writes the user given the sessionUser to the file
	 * @param temp User
	 * @param context
	 * @throws IOException
	 */
	public static void writeUser(User temp, Context context) throws IOException {
		File yourFile = new File(context.getFilesDir() + File.separator +  storeFile);
		yourFile.createNewFile();
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(context.getFilesDir() + File.separator + storeFile));
		oos.writeObject(temp);
		oos.close();
	}

	/**
	 * Reads the user from the file
	 * @return user
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static User readUser(Context context) throws IOException, ClassNotFoundException {
		User retUser = null;
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(context.getFilesDir() + File.separator + storeFile));
			retUser = (User)ois.readObject();
			try {
				Log.d(TAG, "readUser(): reading results" + retUser.getAlbums().get(0).getName());
			}
			catch(Exception e){
				e.printStackTrace();
			}
				ois.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return retUser;
	}

	// public static void printUsers(){
	// for(User user: userList){
	// System.out.println("User:"+user.username+"
	// Password:"+user.getPassword()+" List:"+user.getAlbums());
	// }
	// }

}
