package com.example.ronishdesai.photoalbumapp;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SearchView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class SearchPage extends AppCompatActivity {
    private static final String TAG = MainPage.class.getSimpleName();
    Context context;
    Session session;
    ImageListAdapter adapter;
    GridView gridView;
    ArrayList<MyImage> data;
    String query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = SearchPage.this;
        session = new Session(context);
        setContentView(R.layout.activity_search_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);

        Intent intent = getIntent();
        if (Intent.ACTION_ANSWER.equals(intent.getAction())) {
            Bundle extras = getIntent().getExtras();
            if(extras != null) {
                query = extras.getString("query");
            }
            ArrayList<MyImage> results = session.sessionUser.searchImages(query);
            data = results;
        } else {
            data = new ArrayList<MyImage>();
        }

        adapter = new ImageListAdapter(this, R.layout.content_image_item, data);
        adapter.notifyDataSetChanged();
        gridView = (GridView)findViewById(R.id.grid_result);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
//              MyImage item = (MyImage) parent.getItemAtPosition(position);
//                Intent intent = new Intent(context, DetailsPage.class);
//                intent.putExtra("position", position);
//                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Intent intent = new Intent(SearchPage.this, SearchableActivity.class);
                intent.setAction(Intent.ACTION_SEARCH);
                intent.putExtra(SearchManager.QUERY,s);
                startActivity(intent);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                session.sessionUser.getSuggestions(s);
                return false;
            }
        });
        return true;
//        getMenuInflater().inflate(R.menu.search_menu, menu);
//        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
//        //searchView.setOnQueryTextListener(this);
//        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            UserInfo.writeUser(session.sessionUser, context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    public void onBackPressed() {
        try {
            UserInfo.writeUser(session.sessionUser, context);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(SearchPage.this, MainPage.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
