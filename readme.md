In order for the permission to work properly, you need to do the following:
Go to “Settings”
Then go to “App”
Then go to “uPhoto”
Then go to “Permission” 
And check “camera” and/or “storage” 
This will avoid the permission denied error and the app works fine from here onwards.